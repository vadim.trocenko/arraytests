const MyArray = require('../index.js');


let arr;

describe('test for some method', () => {
  beforeEach(() => {
    arr = new MyArray(1, 2, 3, 4, 5);
  });

  test('1 instance has not Own Property some', () => {
    expect(arr.hasOwnProperty('some')).toBeFalsy();
  });

  test('2 instance has method some', () => {
    expect(arr).toHaveProperty('some');
  });

  test('3 the method should return boolean type', () => {
    const resultArr = arr.some(el => el === 1);


    expect(typeof(resultArr)).toBe('boolean');
  });

  test('4 the method should not mutate original array', () => {
    arr.some(el => el === 1);
    expect(arr).toEqual([1, 2, 3, 4, 5]);
  });

  test('5 the method called on an empty array, should return false', () => {
    arr = new MyArray();
    const result = arr.some(el => el === 1);

    
    expect(result).toBe(false);
  });

  test('6 if callback does not find true value, method should return false ', () => {
    const result = arr.some(el => el > 100);

    
    expect(result).toBe(false);
  });

  test('7 the method should passed first argument callback with 3 arguments(element, index, array)', () => {
    const mockFn = jest.fn();


    arr.some(mockFn);
    expect(mockFn).toHaveBeenCalledWith(1, 0, arr);
    expect(mockFn).toHaveBeenCalledWith(2, 1, arr);
    expect(mockFn).toHaveBeenCalledWith(3, 2, arr);
    expect(mockFn).toHaveBeenCalledWith(4, 3, arr);
    expect(mockFn).toHaveBeenCalledWith(5, 4, arr);
  });

  test('8 the method uses the second argument thisArg as context during the callback', () => {
    const mockFn = jest.fn().mockReturnThis();
    const thisArg = {a: 2};


    arr.some(mockFn, thisArg);
    expect(mockFn).toHaveNthReturnedWith(1, thisArg);
  });

  test('9 the method\'s callback should be called with every not empty element of array', () => {
    const mockFn = jest.fn(); 


    delete arr[1];
    arr.some(mockFn);
    expect(mockFn).toHaveBeenCalledWith(1, 0, arr);
    expect(mockFn).toHaveBeenCalledWith(3, 2, arr);
    expect(mockFn).toHaveBeenCalledWith(4, 3, arr);
    expect(mockFn).toHaveBeenCalledWith(5, 4, arr);
    expect(mockFn).toHaveBeenCalledTimes(4);
  });

  test('10 the method return true when callback find the first true value', () => {
    const mockFn = jest.fn(el => el === 2);


    expect(arr.some(mockFn)).toBe(true);
    expect(mockFn).toHaveBeenCalledWith(1, 0, arr);
    expect(mockFn).toHaveBeenCalledWith(2, 1, arr);
    expect(mockFn).toHaveBeenCalledTimes(2);
  });

  test('11 the method has property length which equal 1', () => {
    expect(arr.some).toHaveLength(1);
  });

  test('12 the method\'s callback should calls only once for every not empty element', () => {
    const mockFn = jest.fn(el => el === 222);


    arr.some(mockFn);
    expect(mockFn).toHaveBeenCalledWith(1, 0, arr);
    expect(mockFn).toHaveBeenCalledWith(2, 1, arr);
    expect(mockFn).toHaveBeenCalledWith(3, 2, arr);
    expect(mockFn).toHaveBeenCalledWith(4, 3, arr);
    expect(mockFn).toHaveBeenCalledWith(5, 4, arr);
    expect(mockFn).toHaveBeenCalledTimes(5);
  });
});