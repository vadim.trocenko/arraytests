const MyArray = require('../index.js');


let arr;

describe('test for map method', () => {
  beforeEach(() => {
    arr = new MyArray(1, 2, 3);
  });

  test('1 instance has not Own property map', () => { 
    expect(arr.hasOwnProperty('map')).toBeFalsy();
  });

  test('2 instance has method map', () => {
    expect(arr).toHaveProperty('map');
  });

  test('3 the method uses the second argument thisArg as context during the callback', () => {
    const mockFn = jest.fn().mockReturnThis();
    const thisArg = {a: 2};


    arr.map(mockFn, thisArg);
    expect(mockFn).toHaveNthReturnedWith(1, thisArg);
  });
  
  test('4 the method should return new array', () => {
    const resultArr = arr.map(el => el * 2);


    expect(resultArr).toBeInstanceOf(MyArray);
    expect(resultArr).not.toBe(arr);
    expect(resultArr).toEqual([2, 4, 6]);
  });

  test('5 the method should not mutate original array', () => {
    arr.map(item => item + 1);
    expect(arr).toEqual([1, 2, 3]);
  });

  test('6 the method should pass in callback 3 arguments (item, index, array)', () => {
    const mockFn = jest.fn();


    arr.map(mockFn);
    expect(mockFn).toHaveBeenCalledWith(1, 0, arr);
    expect(mockFn).toHaveBeenCalledWith(2, 1, arr);
    expect(mockFn).toHaveBeenCalledWith(3, 2, arr);
  });

  test('7 the method callback is called only for indexes that have assigned values, including undefined, null, empty string', () => {
    const arr = new MyArray(1, 2, 3,'',null,undefined);
    const mockFn = jest.fn(x => x);


    arr.map(mockFn);
    expect(mockFn).toHaveBeenCalledWith(1, 0, arr);
    expect(mockFn).toHaveBeenCalledWith(2, 1, arr);
    expect(mockFn).toHaveBeenCalledWith(3, 2, arr);
    expect(mockFn).toHaveBeenCalledWith('', 3, arr);
    expect(mockFn).toHaveBeenCalledWith(null, 4, arr);
    expect(mockFn).toHaveBeenCalledWith(undefined, 5, arr);
    expect(mockFn).toHaveBeenCalledTimes(6);
  });

  test('8 the length of the new array is equal to the length of an array on which the method was called', () => {
    const resultArr = arr.map(x => x + 2);


    expect(resultArr).toHaveLength(3);
  });

  test('9 the method\'s callback should called with every not empty item of array', () => {
    const mockFn = jest.fn();
    const arrWithEmpty = new MyArray(1, 2, 3, 4);
    arrWithEmpty.length = 6;


    arrWithEmpty.map(mockFn);
    expect(mockFn.mock.calls[0][0]).toBe(1);
    expect(mockFn.mock.calls[1][0]).toBe(2);
    expect(mockFn.mock.calls[2][0]).toBe(3);
    expect(mockFn.mock.calls[3][0]).toBe(4);
    expect(mockFn).toHaveBeenCalledTimes(4);
  });

  test('10 the method has property length which equal 1',() => {
    expect(arr.map).toHaveLength(1);
  });

  test('11 the method call callback only once for each element in ascending order', () => {
    const mockFn = jest.fn();


    arr.map(mockFn);
    expect(mockFn).toHaveBeenCalledWith(1, 0, arr);
    expect(mockFn).toHaveBeenCalledWith(2, 1, arr);
    expect(mockFn).toHaveBeenCalledWith(3, 2, arr);
    expect(mockFn).toHaveBeenCalledTimes(3);
  });

  test('12 the method callback won\'t work with new value if they added when method work', () => {
    const mockFn = jest.fn((item,index) => {
      if (index === 0) {
        arr.push(1);
        arr.push(2);
      };
      return item;
    });


    arr.map(mockFn);
    expect(mockFn).toHaveBeenCalledTimes(3);
  });
});