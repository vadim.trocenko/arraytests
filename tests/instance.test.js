const MyArray = require('../index.js');


let arr;

describe('test for instance', () => {
  beforeEach(() => {
    arr = new MyArray(1, 2, 3);
  });

  test('1 last item of array is equal array\'s length minus 1', () => {
    expect(arr[arr.length - 1]).toBe(3);
  });

  test('2 property length return the biggest index of the array plus 1 ', () => {
    expect(arr).toHaveLength(3);
    arr.push(4);
    expect(arr).toHaveLength(4);
  });

  describe('3 test for length of instance', () => {
    test('3.1 length of instance should be equal to the number of elements', () => {
      expect(arr).toHaveLength(3);
    });

    test('3.2 if to property length is assigned 0 should be empty error', () => {
      arr.length = 0;
      expect(arr).toEqual([]);
    });

    test('3.3 if to property length is assigned negative number throw Error', () => {
    expect(() => arr.length = -2).toThrow();
    });

    test('3.4 if to property length is assigned string value which can\'t convert to number throw Error', () => {
      expect(() => arr.length = 'a').toThrow();
    });

    test('3.5 if to property length is assigned object which can\'t convert to number throw Error', () => {
      expect(() => arr.length = {a: 'a'}).toThrow();
    });

    test('3.6 if to property length is assigned undefined throw Error', () => {
      expect(() => arr.length = undefined).toThrow();
    });

    test('3.7 if change the length value by more than it was, then new undefined elements will add at the end', () => {
      arr.length = 4;
      expect(arr).toEqual([1, 2, 3, undefined]);
    });
  });
});