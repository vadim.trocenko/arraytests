const MyArray = require('../index.js');


let arr;

describe('test for toString method', () => {
  beforeEach(() => {
    arr = new MyArray(1, 2, 3, 4, 5);
  });

  test('1 instance has not Own Property toString', () => {
    expect(arr.hasOwnProperty('toString')).toBeFalsy();
  });
  
  test('2 instance has method toString', () => {
    expect(arr).toHaveProperty('toString');
  });
  
  test('3 the method should not mutate original array', () => {
    arr.toString();
    expect(arr).toEqual([1, 2, 3, 4, 5]);
  });

  test('4 the method should return string value', () => {
    const resultArr = arr.toString();


    expect(typeof resultArr).toBe('string');
  });

  test('5 if call the method on an empty array with the assigned length, result should be separate by comma every empty element', () => {
    const resultArr = new MyArray(5);


    expect(resultArr.toString()).toBe(',,,,');
  });

  test('6 the method should convert to string every element of array', () => {
    const resultArr = new MyArray(null, { name : 'Roma' }, 100, 'Vova', { role : 'driver' });


    expect(resultArr.toString()).toBe(',[object Object],100,Vova,[object Object]');
  });

  test('7 the method should replace null, undefined on empties which separates by comma', () => {
    const resultArr = new MyArray(null, 10, undefined, 10);


    expect(resultArr.toString()).toBe(',10,,10');
  });
  
  test('8 if method call on empty array, method should return empty string', () => {
    const resultArr = new MyArray();

    
    expect(resultArr.toString()).toBe('');
  });
  
  test('9 the method should use a comma separator for each element in the resulting string', () => {
    const resultArr = arr.toString();


    expect(resultArr).toBe('1,2,3,4,5');
  });

  test('10 the method has property length which equal 0', () => {
    expect(arr.toString).toHaveLength(0);
  });
});