const MyArray = require('../index.js');


let arr;

describe('test for reduce method', () => {
  beforeEach(() => {
    arr = new MyArray(1, 2, 3);
  });

  test('1 instance has not Own Property reduce', () => {
    expect(arr.hasOwnProperty('reduce')).toBeFalsy();
  });

  test('2 instance has method reduce', () => {
    expect(arr).toHaveProperty('reduce');
  });

  test('3 if in the method second argument(initial value) is passed and the array not empty, then should be equal first argument of callback(previous value) on first iteration', () => {
    const mockFn = jest.fn();


    arr.reduce(mockFn, 222);
    expect(mockFn.mock.calls[0][0]).toBe(222);
  });

  test('4 the method callback second argument(current value) should equal the first value in the array on first iteration if method\'s second argument(initial value) is passed', () => {
    const mockFn = jest.fn();


    arr.reduce(mockFn, 222);
    expect(mockFn.mock.calls[0][1]).toBe(1);
  });

  test('5 the method has property length which equal 1', () => {
    expect(arr.reduce).toHaveLength(1);
  });

  test('6 if the array is empty the method don\'t call callback instead of this method should return second method\'s argument(initial value) if it passed', () => {
    const mockFn = jest.fn();
    const emptyArr = new MyArray();


    
    expect(mockFn).toHaveBeenCalledTimes(0);
    expect(emptyArr.reduce(() => mockFn, 1)).toBe(1);
  });

  test('7 if the array is empty and the second argument(initial value) is not set, it will throw a TypeError', () => {
    const emptyArr = new MyArray();


    expect(() => emptyArr.reduce(() => {})).toThrow(TypeError);
  });

  test('8 the method\'s callback should called with every not empty item of array', () => {
    const mockFn = jest.fn();
    const arrWithEmpty = new MyArray(1, 2, 3, 4);
    arrWithEmpty.length = 6;


    arrWithEmpty.reduce(mockFn);
    expect(mockFn.mock.calls[0][0]).toBe(1);
    expect(mockFn.mock.calls[0][1]).toBe(2);
    expect(mockFn.mock.calls[1][1]).toBe(3);
    expect(mockFn.mock.calls[2][1]).toBe(4);
    expect(mockFn).toHaveBeenCalledTimes(3);
  });

  test('9 the method callback should take index as third argument and array as fourth', () => {
    const mockFn = jest.fn((prev, current) => current);


    arr.reduce(mockFn);
    expect(mockFn).toHaveBeenCalledWith(1, 2, 1, arr);
    expect(mockFn).toHaveBeenCalledWith(2, 3, 2, arr);
  });

  test('10 the method callback is called only for indexes that have assigned value, including undefined, null, empty string', () => {
    const mockFn = jest.fn((prev, current) => current);
    const arr = new MyArray(1, 2, 3,null,undefined,'');


    arr.reduce(mockFn);
    expect(mockFn).toHaveBeenCalledWith(1, 2, 1, arr);
    expect(mockFn).toHaveBeenCalledWith(2, 3, 2, arr);
    expect(mockFn).toHaveBeenCalledWith(3, null, 3, arr);
    expect(mockFn).toHaveBeenCalledWith(null, undefined, 4, arr);
    expect(mockFn).toHaveBeenCalledWith(undefined, '', 5, arr);
    expect(mockFn).toHaveBeenCalledTimes(5);
  });

  test('11 the method should not mutate original array', () => {
     arr.reduce((prev, current) => prev + current);
    expect(arr).toEqual([1, 2, 3]);
  });

  test('12 the method\'s callback shouldn\'t be invoked with new value if they have been added while it\'s invocation', () => {
    const mockFn = jest.fn((previousValue, current) => previousValue * current);


    arr.reduce((prev, current, index) => {
      if (index === 1) {
        arr.push(1);
        arr.push(2);
        arr.push(3);
        arr.push(4);
      }
      return mockFn(prev, current);
    });
    expect(mockFn).toHaveBeenCalledTimes(2);
  });

  test('13 if second argument(initial value) is set in method,method should start iteration by array from index 0 to last index of array(array.length - 1)', () => {
    const mockFn = jest.fn((prev, current) => current);


    arr.reduce(mockFn, 222);
    expect(mockFn).toHaveBeenCalledWith(222, 1, 0, arr);
    expect(mockFn).toHaveBeenCalledWith(1, 2, 1, arr);
    expect(mockFn).toHaveBeenCalledWith(2, 3, 2, arr);
    expect(mockFn).toHaveBeenCalledTimes(3);
  });

});