const MyArray = require('../index.js');


let arr;

describe('test for slice method', () => {
  beforeEach(() => {
    arr = new MyArray(1, 2, 3);
  });

  test('1 instance has not Own Property slice', () => {
    expect(arr.hasOwnProperty('slice')).toBeFalsy();
  });

  test('2 instance has method slice', () => {
    expect(arr).toHaveProperty('slice');
  });

  test('3 the method should not mutate original array', () => {
    arr.slice(1, 2);
    expect(arr).toEqual([1, 2, 3]);
  });

  test('4 if no arguments in method return copy with all array items', () => {
    expect(arr.slice()).toEqual([1, 2, 3]);
  });

  test('5 if the first argument in method is greater than the length of the array, return an empty array', () => {
    expect(arr.slice(5)).toEqual([]);
  });

  test('6 the method has property length which equal 2', () => {
    expect(arr.slice).toHaveLength(2);
  });

  test('7 if passed only one argument(start) and don\'t have second argument(end) in the method, will copy all elements to end', () => {
    expect(arr.slice(2)).toEqual([3]);
  });

  test('8 if passed only one negative argument(start), it should copy from arr.length + begin to end', () => {
    expect(arr.slice(-1)).toEqual([3]);
  });

  test('9 if passed first argument(start) and second negative(end), it should copy from begin to arr.length + end', () => {
    expect(arr.slice(1, -1)).toEqual([2]);
  });

  test('11 if passed first argument(start) and second(end), it should copy from begin to end don\'t include it', () => {
    expect(arr.slice(1, 2)).toEqual([2]);
  });
});