const MyArray = require('../index.js');


let arr;

describe('test for constructor', () => {
  beforeEach(() => {
    arr = new MyArray(1, 2, 3);
  });
  
  test('1 if values passed to the constructor should be passed to the instance in the same order', () => {
    expect(arr).toEqual([1, 2, 3]);
  });

  test('2 it is possible to create a new instance when constructor called as a function', () => {
    expect(MyArray(1, 2, 3)).toBeInstanceOf(MyArray);
  });

  test('3 if pass only one whole positive number, instance length should be this number', () => {
    expect(new MyArray(3)).toHaveLength(3);
  });

  test('4 if constructor called with only one number which is a equal or greater than 2**32, should throw Error', () => {
    expect(() => new MyArray(2**32)).toThrow();
  });
});